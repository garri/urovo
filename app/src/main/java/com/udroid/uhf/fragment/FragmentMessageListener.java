package com.udroid.uhf.fragment;

public interface FragmentMessageListener {
    void sendMessage(String message);
}
