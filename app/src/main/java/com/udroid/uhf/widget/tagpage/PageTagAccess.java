package com.udroid.uhf.widget.tagpage;


import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.bean.OperateBean;
import com.rfid.RFIDReaderHelper;
import com.rfid.config.CMD;
import com.rfid.config.ERROR;
import com.rfid.rxobserver.RXObserver;
import com.rfid.rxobserver.ReaderSetting;
import com.rfid.rxobserver.bean.RXOperationTag;
import com.udroid.uhf.R;
import com.udroid.uhf.UHFApplication;
import com.udroid.uhf.activity.MainActivity;
import com.udroid.uhf.adapter.AbstractSpinerAdapter.IOnItemSelectListener;
import com.udroid.uhf.dialog.SpinerPopWindow;
import com.udroid.uhf.widget.HexEditTextBox;
import com.udroid.uhf.widget.LogList;
import com.udroid.uhf.widget.TagAccessList;
import com.util.FormatUtils;
import com.util.PreferenceUtil;
import com.util.StringTool;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


public class PageTagAccess extends LinearLayout {

    //fixed by lei.li 2016/11/09
    //private LogList mLogList;
    private LogList mLogList;
    //fixed by lei.li 2016/11/09

    private TextView mGet, mRead, mSelect, mWrite, mLock, mKill;

    //private TextView mRefreshButton;

    private TextView mTagAccessListText;
    private TableRow mDropDownRow;

    private List<String> mAccessList;
    private SpinerPopWindow mSpinerPopWindow;

    private HexEditTextBox mPasswordEditText;
    private EditText mStartAddrEditText;
    private EditText mDataLenEditText;
    private HexEditTextBox mDataEditText;
    private HexEditTextBox mLockPasswordEditText;
    private HexEditTextBox mKillPasswordEditText;
    private EditText mPC, mCRC;
    private LinearLayout llEpc;

    private RadioGroup mGroupAccessAreaType;
    private RadioGroup mGroupLockAreaType;
    private RadioGroup mGroupLockType;

    private TagAccessList mTagAccessList;

    private RFIDReaderHelper mReaderHelper;

    private int mPos = -1;

    private static ReaderSetting m_curReaderSetting;

    private Handler mHandler;
    private Context mContext;
    private UHFApplication mApp;

    private boolean isOpen = false;

    public PageTagAccess(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        LayoutInflater.from(context).inflate(R.layout.page_tag_access, this);
        mApp = (UHFApplication) ((Activity) context).getApplication();
        try {
            mReaderHelper = RFIDReaderHelper.getDefaultHelper();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        isOpen = PreferenceUtil.getBoolean("data_open", false);

        mAccessList = new ArrayList<>();
        mHandler = new Handler();

        m_curReaderSetting = ReaderSetting.newInstance();

        mPC = findViewById(R.id.data_pc);
        mCRC = findViewById(R.id.data_crc);
        llEpc = (LinearLayout) findViewById(R.id.ll_epc);
        mLogList = (LogList) findViewById(R.id.log_list);
        mGet = (TextView) findViewById(R.id.get);
        mRead = (TextView) findViewById(R.id.read);
        mSelect = (TextView) findViewById(R.id.select);
        mWrite = (TextView) findViewById(R.id.write);
        mLock = (TextView) findViewById(R.id.lock);
        mKill = (TextView) findViewById(R.id.kill);
        mGet.setOnClickListener(setAccessOnClickListener);
        mRead.setOnClickListener(setAccessOnClickListener);
        mSelect.setOnClickListener(setAccessOnClickListener);
        mWrite.setOnClickListener(setAccessOnClickListener);
        mLock.setOnClickListener(setAccessOnClickListener);
        mKill.setOnClickListener(setAccessOnClickListener);

        mPasswordEditText = (HexEditTextBox) findViewById(R.id.password_text);
        mStartAddrEditText = (EditText) findViewById(R.id.start_addr_text);
        mDataLenEditText = (EditText) findViewById(R.id.data_length_text);
        mDataEditText = (HexEditTextBox) findViewById(R.id.data_write_text);
        mLockPasswordEditText = (HexEditTextBox) findViewById(R.id.lock_password_text);
        mKillPasswordEditText = (HexEditTextBox) findViewById(R.id.kill_password_text);

        mGroupAccessAreaType = (RadioGroup) findViewById(R.id.group_access_area_type);
        mGroupAccessAreaType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                Log.v("urovo", "position:" + i);
                if (i == R.id.set_access_area_epc && isOpen) {
                    if (llEpc.getVisibility() == GONE)
                        llEpc.setVisibility(VISIBLE);
                } else {
                    if (llEpc.getVisibility() == VISIBLE)
                        llEpc.setVisibility(GONE);
                }
            }
        });
        mGroupLockAreaType = (RadioGroup) findViewById(R.id.group_lock_area_type);
        mGroupLockType = (RadioGroup) findViewById(R.id.group_lock_type);

        mTagAccessListText = (TextView) findViewById(R.id.tag_access_list_text);
        mDropDownRow = (TableRow) findViewById(R.id.table_row_tag_access_list);

        mTagAccessList = (TagAccessList) findViewById(R.id.tag_access_list);

        mDropDownRow.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                showSpinWindow();
            }
        });

        mAccessList.clear();
        mAccessList.add("Cancel");
        mAccessList.addAll(mApp.getTags());

        mSpinerPopWindow = new SpinerPopWindow(mContext);
        mSpinerPopWindow.refreshData(mAccessList, 0);
        mSpinerPopWindow.setItemListener(new IOnItemSelectListener() {
            @Override
            public void onItemClick(int pos) {
                setAccessSelectText(pos);
            }
        });

        updateView();

        ((MainActivity) mContext).mOperateList = mTagAccessList.getData();
//		mRefreshButton = (TextView) findViewById(R.id.refresh);
//		mRefreshButton.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View arg0) {
//				refresh();
//			}
//		});
    }

    public void refresh() {
        mTagAccessListText.setText("");
        mPos = -1;

        mStartAddrEditText.setText("");
        mDataLenEditText.setText("");
        mDataEditText.setText("");
        mPasswordEditText.setText("");
        mLockPasswordEditText.setText("");
        mKillPasswordEditText.setText("");

        mGroupAccessAreaType.check(R.id.set_access_area_password);
        mGroupLockAreaType.check(R.id.set_lock_area_access_password);
        mGroupLockType.check(R.id.set_lock_free);
        mApp.clearTags();

        //add by lei.li 2017/1/16
        mAccessList.clear();
        mAccessList.add("cancel");
        //add by lei.li 2017/1/16
        mTagAccessList.clearData();
        isOpen = PreferenceUtil.getBoolean("data_open", false);
        Log.v("urovo", ">>>>>>>>>>>更新" + isOpen + ">>>>>>>>>");
        if (mGroupAccessAreaType.getCheckedRadioButtonId() == R.id.set_access_area_epc && isOpen) {
            if (llEpc.getVisibility() == GONE) {
                llEpc.setVisibility(VISIBLE);
            }
        } else if (mGroupAccessAreaType.getCheckedRadioButtonId() == R.id.set_access_area_epc && !isOpen) {
            if (llEpc.getVisibility() == VISIBLE) {
                llEpc.setVisibility(GONE);
            }
        }
    }

    private void setAccessSelectText(int pos) {
        if (pos >= 0 && pos < mAccessList.size()) {
            String value = mAccessList.get(pos);
            mTagAccessListText.setText(value);
            mPos = pos;
        }
    }

    private void showSpinWindow() {
        for (int i = 0; i < mApp.getTags().size(); i++) {
            if (!mAccessList.contains(mApp.getTags().get(i)))
                mAccessList.add(mApp.getTags().get(i));
        }
        mSpinerPopWindow.refreshData(mAccessList, 0);
        mSpinerPopWindow.setWidth(mDropDownRow.getWidth());
        mSpinerPopWindow.showAsDropDown(mDropDownRow);
    }

    private void updateView() {
        if (mPos < 0) mPos = 0;
        setAccessSelectText(mPos);
    }

    private OnClickListener setAccessOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            switch (arg0.getId()) {
                case R.id.get: {
                    mReaderHelper.getAccessEpcMatch(m_curReaderSetting.btReadId);
                    break;
                }
                case R.id.select: {
                    if (mPos <= 0) {
                        mReaderHelper.cancelAccessEpcMatch(m_curReaderSetting.btReadId);
                    } else {
                        byte[] btAryEpc = null;

                        try {
                            String[] result = StringTool.stringToStringArray(mAccessList.get(mPos).toUpperCase(), 2);
                            btAryEpc = StringTool.stringArrayToByteArray(result, result.length);
                        } catch (Exception e) {
                            Toast.makeText(
                                    mContext,
                                    getResources().getString(R.string.param_unknown_error),
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }

                        if (btAryEpc == null) {
                            Toast.makeText(
                                    mContext,
                                    getResources().getString(R.string.param_unknown_error),
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }

                        mReaderHelper.setAccessEpcMatch(m_curReaderSetting.btReadId, (byte) (btAryEpc.length & 0xFF), btAryEpc);
                    }
                    break;
                }
                case R.id.read:
                case R.id.write: {
                    byte btMemBank = 0x00;
                    byte btWordAdd = 0x00;
                    byte btWordCnt = 0x00;
                    byte[] btAryPassWord = null;
                    if (mGroupAccessAreaType.getCheckedRadioButtonId() == R.id.set_access_area_password) {
                        btMemBank = 0x00;
                    } else if (mGroupAccessAreaType.getCheckedRadioButtonId() == R.id.set_access_area_epc) {
                        btMemBank = 0x01;
                    } else if (mGroupAccessAreaType.getCheckedRadioButtonId() == R.id.set_access_area_tid) {
                        btMemBank = 0x02;
                    } else if (mGroupAccessAreaType.getCheckedRadioButtonId() == R.id.set_access_area_user) {
                        btMemBank = 0x03;
                    }

                    try {
                        btWordAdd = (byte) Integer.parseInt(mStartAddrEditText.getText().toString());
                    } catch (Exception e) {
                        Toast.makeText(
                                mContext,
                                getResources().getString(R.string.param_start_addr_error),
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                    try {
                        String[] reslut = StringTool.stringToStringArray(mPasswordEditText.getText().toString().toUpperCase(), 2);
                        btAryPassWord = StringTool.stringArrayToByteArray(reslut, 4);
                    } catch (Exception e) {
                        Toast.makeText(
                                mContext,
                                getResources().getString(R.string.param_password_error),
                                Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (arg0.getId() == R.id.read) {
                        try {
                            if (PreferenceUtil.getBoolean("data_open", false)) {
                                final int read_length = Integer.parseInt(mDataLenEditText.getText().toString());
                                btWordCnt = (byte) (read_length / 2 + read_length % 2);
                            } else {
                                btWordCnt = (byte) (Integer.parseInt(mDataLenEditText.getText().toString()));
                            }
                        } catch (Exception e) {
                            Toast.makeText(
                                    mContext,
                                    getResources().getString(R.string.param_data_len_error),
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }

                        if ((btWordCnt & 0xFF) <= 0) {
                            Toast.makeText(
                                    mContext,
                                    getResources().getString(R.string.param_data_len_error),
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }

                        mTagAccessList.clearData();
                        mReaderHelper.readTag(m_curReaderSetting.btReadId, btMemBank, btWordAdd, btWordCnt, btAryPassWord);
                    } else {
                        final byte[] btAryData;
                        String[] result = null;
                        byte[] btPC = null;
                        byte[] btCRC = null;
                        final int data_length;
                        if (PreferenceUtil.getBoolean("data_open", false)) { //ASCII自动转换打开
                            if (btWordAdd == 0x00 || btWordAdd == 0x01) {
                                Toast.makeText(mContext, "写入数据起始地址请从第二位开始", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            final String strData = mDataEditText.getText().toString().toUpperCase();
                            data_length = strData.length() % 2 == 0 ? strData.length() / 2 : strData.length() / 2 + 1;
                            if (TextUtils.isEmpty(strData)) {
                                Toast.makeText(mContext, "请填入写入数据", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            final byte[] btData = strData.getBytes();
                            if (btMemBank == 0x01 && btWordAdd == 0x02) { //写入epc
                                final String strPC = mPC.getText().toString().toUpperCase();
                                final String strCRC = mCRC.getText().toString().toUpperCase();
                                if (!TextUtils.isEmpty(strCRC) && TextUtils.isEmpty(strPC)) {
                                    Toast.makeText(mContext, "请填入对应的PC区域数据", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                if (!TextUtils.isEmpty(strPC) && strPC.length() != 4) {
                                    Toast.makeText(mContext, "填入4个长度的PC值", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                if (!TextUtils.isEmpty(strPC)) {
                                    final String[] strByte = StringTool.stringToStringArray(strPC, 2);
                                    btPC = StringTool.stringArrayToByteArray(strByte, strByte.length);
                                } else {
                                    btPC = StringTool.hexStringToBytes(getPCValue(data_length));
                                }
                                if (!TextUtils.isEmpty(strCRC) && strCRC.length() != 4) {
                                    Toast.makeText(mContext, "填入4个长度的CRC值", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                if (!TextUtils.isEmpty(strCRC)) {
                                    final String[] strByte = StringTool.stringToStringArray(strCRC, 2);
                                    btCRC = StringTool.stringArrayToByteArray(strByte, strByte.length);
                                }
                                if (btCRC == null && btPC != null) {
                                    btAryData = new byte[data_length * 2 + 2];
                                } else {
                                    btAryData = new byte[data_length * 2 + 4];
                                }
                                int index = 0;
                                if (btCRC != null) {
                                    System.arraycopy(btCRC, 0, btAryData, 0, 2); //拷贝CRC数据
                                    index += 2;
                                }
                                if (btPC != null) {
                                    System.arraycopy(btPC, 0, btAryData, index, 2);
                                    index += 2;
                                }
                                System.arraycopy(btData, 0, btAryData, index, btData.length);
                                if (strData.length() % 2 != 0) {
                                    final byte[] btAppend = new byte[]{0x00};
                                    System.arraycopy(btAppend, 0, btAryData, index + btData.length, 1);
                                }
                                if (btCRC == null) {
                                    btWordCnt = (byte) (data_length + 1);
                                } else {
                                    btWordCnt = (byte) (data_length + 2);
                                }
                                mReaderHelper.writeTag(m_curReaderSetting.btReadId, btAryPassWord, btMemBank, (byte) (btCRC == null ? 0x01 : 0x00), btWordCnt, btAryData);
                            } else {
                                btAryData = new byte[data_length * 2];
                                System.arraycopy(btData, 0, btAryData, 0, btData.length);
                                if (strData.length() % 2 != 0) {
                                    final byte[] btAppend = new byte[]{0x00};
                                    System.arraycopy(btAppend, 0, btAryData, btData.length, 1);
                                }
                                btWordCnt = (byte) (data_length & 0xFF);
                                mReaderHelper.writeTag(m_curReaderSetting.btReadId, btAryPassWord, btMemBank, btWordAdd, btWordCnt, btAryData);
                            }
                        } else {
                            try {
                                result = StringTool.stringToStringArray(mDataEditText.getText().toString().toUpperCase(), 2);
                                btAryData = StringTool.stringArrayToByteArray(result, result.length);
                                btWordCnt = (byte) ((result.length / 2 + result.length % 2) & 0xFF);
                            } catch (Exception e) {
                                Toast.makeText(
                                        mContext,
                                        getResources().getString(R.string.param_data_error),
                                        Toast.LENGTH_SHORT).show();
                                return;
                            }

                            if (btAryData == null || btAryData.length <= 0) {
                                Toast.makeText(
                                        mContext,
                                        getResources().getString(R.string.param_data_error),
                                        Toast.LENGTH_SHORT).show();
                                return;
                            }

                            if (btAryPassWord == null || btAryPassWord.length < 4) {
                                Toast.makeText(
                                        mContext,
                                        getResources().getString(R.string.param_password_error),
                                        Toast.LENGTH_SHORT).show();
                                return;
                            }

                            mDataLenEditText.setText(String.valueOf(btWordCnt & 0xFF));

                            mTagAccessList.clearData();
                            mReaderHelper.writeTag(m_curReaderSetting.btReadId, btAryPassWord, btMemBank, btWordAdd, btWordCnt, btAryData);
                        }
                    }

                    break;
                }
                case R.id.lock: {
                    byte btMemBank = 0x00;
                    byte btLockType = 0x00;
                    byte[] btAryPassWord = null;
                    if (mGroupLockAreaType.getCheckedRadioButtonId() == R.id.set_lock_area_access_password) {
                        btMemBank = 0x04;
                    } else if (mGroupLockAreaType.getCheckedRadioButtonId() == R.id.set_lock_area_kill_password) {
                        btMemBank = 0x05;
                    } else if (mGroupLockAreaType.getCheckedRadioButtonId() == R.id.set_lock_area_epc) {
                        btMemBank = 0x03;
                    } else if (mGroupLockAreaType.getCheckedRadioButtonId() == R.id.set_lock_area_tid) {
                        btMemBank = 0x02;
                    } else if (mGroupLockAreaType.getCheckedRadioButtonId() == R.id.set_lock_area_user) {
                        btMemBank = 0x01;
                    }

                    if (mGroupLockType.getCheckedRadioButtonId() == R.id.set_lock_free) {
                        btLockType = 0x00;
                    } else if (mGroupLockType.getCheckedRadioButtonId() == R.id.set_lock_free_ever) {
                        btLockType = 0x02;
                    } else if (mGroupLockType.getCheckedRadioButtonId() == R.id.set_lock_lock) {
                        btLockType = 0x01;
                    } else if (mGroupLockType.getCheckedRadioButtonId() == R.id.set_lock_lock_ever) {
                        btLockType = 0x03;
                    } else if (mGroupLockType.getCheckedRadioButtonId() == R.id.set_lock_lock_ever_r6) {
                        btLockType = 0x06;
                    }

                    try {
                        String[] reslut = StringTool.stringToStringArray(mLockPasswordEditText.getText().toString().toUpperCase(), 2);
                        btAryPassWord = StringTool.stringArrayToByteArray(reslut, 4);
                    } catch (Exception e) {
                        Toast.makeText(
                                mContext,
                                getResources().getString(R.string.param_lockpassword_error),
                                Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (btAryPassWord == null || btAryPassWord.length < 4) {
                        Toast.makeText(
                                mContext,
                                getResources().getString(R.string.param_lockpassword_error),
                                Toast.LENGTH_SHORT).show();
                        return;
                    }

                    mTagAccessList.clearData();
                    mReaderHelper.lockTag(m_curReaderSetting.btReadId, btAryPassWord, btMemBank, btLockType);

                    break;
                }
                case R.id.kill: {
                    byte[] btAryPassWord = null;
                    try {
                        String[] reslut = StringTool.stringToStringArray(mKillPasswordEditText.getText().toString().toUpperCase(), 2);
                        btAryPassWord = StringTool.stringArrayToByteArray(reslut, 4);
                    } catch (Exception e) {
                        Toast.makeText(
                                mContext,
                                getResources().getString(R.string.param_killpassword_error),
                                Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (btAryPassWord == null || btAryPassWord.length < 4) {
                        Toast.makeText(
                                mContext,
                                getResources().getString(R.string.param_killpassword_error),
                                Toast.LENGTH_SHORT).show();
                        return;
                    }

                    mTagAccessList.clearData();
                    mReaderHelper.killTag(m_curReaderSetting.btReadId, btAryPassWord);
                    break;
                }
            }
        }
    };

    private RXObserver mObserver = new RXObserver() {

        @Override
        protected void onOperationTag(final RXOperationTag tag) {
            final OperateBean bean = new OperateBean(tag.strPC, tag.strCRC, tag.strEPC, tag.strData, String.valueOf(tag.nDataLen), tag.btAntId);
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    String strLog = FormatUtils.format(tag.cmd, ERROR.SUCCESS);
                    mLogList.writeLog(strLog, ERROR.SUCCESS);
                    mTagAccessList.addData(bean);
                }
            });
        }

        @Override
        protected void onExeCMDStatus(final byte cmd, final byte status) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (cmd == CMD.GET_ACCESS_EPC_MATCH) {
                        if (status == ERROR.SUCCESS) {
                            mTagAccessListText.setText(m_curReaderSetting.mMatchEpcValue);
                        }
                    }
                    String strLog = FormatUtils.format(cmd, status);
                    mLogList.writeLog(strLog, status);
                }
            });
        }
    };

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mReaderHelper.registerObserver(mObserver);
        mReaderHelper.startWith();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mReaderHelper.unRegisterObserver(mObserver);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mLogList.tryClose()) return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private String getPCValue(int length) {
        String strLength = Integer.toBinaryString(length);
        System.out.println("epc长度二进制：" + strLength);
        final StringBuilder builder = new StringBuilder();
        if (strLength.length() < 5) {
            for (int i = 0; i < 5 - strLength.length(); i++) {
                builder.append("0");
            }
            builder.append(strLength);
        }
        builder.append("00000000000");
        System.out.println("epc长度二进制(16位)：" + builder.toString());
        final StringBuilder integerBuilder = new StringBuilder();
        for (int s = 0; s < builder.length(); ) {
            System.out.println("substring:" + builder.substring(s, s + 4));
            integerBuilder.append(String.valueOf(Integer.valueOf(builder.substring(s, s + 4), 2)));
            s += 4;
        }
        System.out.println("data:" + integerBuilder.toString());
        byte[] data = StringTool.hexStringToBytes(integerBuilder.toString());
        return integerBuilder.toString();
    }
}

