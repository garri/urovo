package com.udroid.uhf.widget.tagpage;


import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bean.Operate6BBean;
import com.rfid.RFIDReaderHelper;
import com.rfid.config.CMD;
import com.rfid.config.ERROR;
import com.rfid.rxobserver.RXObserver;
import com.rfid.rxobserver.ReaderSetting;
import com.udroid.uhf.R;
import com.udroid.uhf.UHFApplication;
import com.udroid.uhf.widget.LogList;
import com.udroid.uhf.widget.TagReal6BList;
import com.util.FormatUtils;


public class PageInventoryReal6B extends LinearLayout {
    //fixed by lei.li 2016/11/09
    //private LogList mLogList;
    private LogList mLogList;
    //fixed by lei.li 2016/11/09
    private TextView mStartStop;

    //private TextView mRefreshButton;

    private TagReal6BList mTagReal6BList;

    private RFIDReaderHelper mReaderHelper;

    private static ReaderSetting m_curReaderSetting;
    /**
     * 是否循环盘存
     */
    private boolean mIsLoop = false;
    private UHFApplication mApp;

    public PageInventoryReal6B(Context context, AttributeSet attrs) {
        super(context, attrs);
        mApp = (UHFApplication) ((Activity) context).getApplication();
        LayoutInflater.from(context).inflate(R.layout.page_inventory_real_6b, this);

        try {
            mReaderHelper = RFIDReaderHelper.getDefaultHelper();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        m_curReaderSetting = ReaderSetting.newInstance();

        mLogList = (LogList) findViewById(R.id.log_list);
        mStartStop = (TextView) findViewById(R.id.startstop6b);

        mTagReal6BList = (TagReal6BList) findViewById(R.id.tag_real_6b_list);

        mStartStop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (arg0.getId() == R.id.startstop6b) {
                    startStop(mStartStop.getText().toString().equals(getResources().getString(R.string.start_inventory)));
                }
            }
        });
    }

    public void refresh() {
        mTagReal6BList.clearData();
    }

    @SuppressWarnings("deprecation")
    private void refreshStartStop(boolean start) {
        if (start) {
            mStartStop.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_disenabled_background));
            mStartStop.setText(getResources().getString(R.string.stop_inventory));
        } else {
            mStartStop.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_background));
            mStartStop.setText(getResources().getString(R.string.start_inventory));
        }
    }

    private Handler mLoopHandler = new Handler();
    private Runnable mLoopRunnable = new Runnable() {
        public void run() {
            mLoopHandler.removeCallbacks(this);
            mReaderHelper.iso180006BInventory(m_curReaderSetting.btReadId);
            mLoopHandler.postDelayed(this, 2000);
        }
    };

    public void startStop(boolean start) {
        refreshStartStop(start);
        mIsLoop = start;
        if (start) {
            mLoopRunnable.run();
        } else {
            mLoopHandler.removeCallbacks(mLoopRunnable);
        }
    }

    private RXObserver mObserver = new RXObserver() {
        @Override
        protected void onInventory6BTag(byte nAntID, final String strUID) {
            final Operate6BBean bean = new Operate6BBean(nAntID, strUID);
            mLoopHandler.post(new Runnable() {
                @Override
                public void run() {
                    mTagReal6BList.addData(bean);
                    mApp.addTag(strUID);
                }
            });
        }

        @Override
        protected void onInventory6BTagEnd(int nTagCount) {
            mLoopHandler.post(new Runnable() {
                @Override
                public void run() {
                    String strLog = FormatUtils.format(CMD.ISO18000_6B_INVENTORY, ERROR.SUCCESS);
                    mLogList.writeLog(strLog, ERROR.SUCCESS);
                    if (mIsLoop) {
                        mLoopRunnable.run();
                    }
                }
            });
        }

        @Override
        protected void onExeCMDStatus(final byte cmd, final byte status) {
            mLoopHandler.post(new Runnable() {
                @Override
                public void run() {
                    String strLog = FormatUtils.format(cmd, status);
                    mLogList.writeLog(strLog, status);
                    if (cmd == CMD.ISO18000_6B_INVENTORY && mIsLoop) {
                        mLoopRunnable.run();
                    }
                }
            });
        }
    };

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mReaderHelper.registerObserver(mObserver);
        mReaderHelper.startWith();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mReaderHelper.unRegisterObserver(mObserver);
        mLoopHandler.removeCallbacks(mLoopRunnable);
        mApp.clearTags();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mLogList.tryClose()) return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}

