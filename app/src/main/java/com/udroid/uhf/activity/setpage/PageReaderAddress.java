package com.udroid.uhf.activity.setpage;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.rfid.RFIDReaderHelper;
import com.rfid.rxobserver.RXObserver;
import com.rfid.rxobserver.ReaderSetting;
import com.udroid.uhf.R.id;
import com.udroid.uhf.R.layout;
import com.udroid.uhf.UHFApplication;
import com.udroid.uhf.activity.BaseActivity;
import com.udroid.uhf.widget.LogList;
import com.util.FormatUtils;

import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

public class PageReaderAddress extends BaseActivity {
	private LogList mLogList;
	
	private TextView mSet;
	
	private EditText mAddressEditText;
	
	private RFIDReaderHelper mReaderHelper;

	private static ReaderSetting m_curReaderSetting;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(layout.page_reader_address);
		((UHFApplication) getApplication()).addActivity(this);
		
		try {
			mReaderHelper = RFIDReaderHelper.getDefaultHelper();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		m_curReaderSetting = ReaderSetting.newInstance();

		mLogList = (LogList) findViewById(id.log_list);
		mSet = (TextView) findViewById(id.set);
		mAddressEditText = (EditText) findViewById(id.address_text);
		
		mSet.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String strReadId = mAddressEditText.getText().toString();				
				if (TextUtils.isEmpty(strReadId))	return ;
				
				byte btNewReadId = (byte) Integer.parseInt(strReadId, 16);
				mReaderHelper.setReaderAddress(m_curReaderSetting.btReadId, btNewReadId);

				m_curReaderSetting.btReadId = btNewReadId;
			}
		});
		
		mAddressEditText.setText(Integer.toHexString(m_curReaderSetting.btReadId & 0xFF).toUpperCase());
	}

	private RXObserver mObserver = new RXObserver() {
		@Override
		protected void onExeCMDStatus(final byte cmd, final byte status) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					String strLog = FormatUtils.format(cmd, status);
					mLogList.writeLog(strLog, status);
				}
			});
		}
	};

	@Override
	protected void onStart() {
		super.onStart();
		mReaderHelper.registerObserver(mObserver);
	}

	@Override
	protected void onResume() {
		mReaderHelper.startWith();
		super.onResume();
	}

	@Override
	protected void onStop() {
		super.onStop();
		mReaderHelper.unRegisterObserver(mObserver);
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (mLogList.tryClose()) return true;
		}

		return super.onKeyDown(keyCode, event);
	}

}

