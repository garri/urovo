package com.udroid.uhf.activity.setpage;


import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.rfid.RFIDReaderHelper;
import com.rfid.rxobserver.RXObserver;
import com.rfid.rxobserver.ReaderSetting;
import com.udroid.uhf.R;
import com.udroid.uhf.R.id;
import com.udroid.uhf.R.layout;
import com.udroid.uhf.UHFApplication;
import com.udroid.uhf.activity.BaseActivity;
import com.udroid.uhf.widget.LogList;
import com.util.BeeperUtils;
import com.util.FormatUtils;
import com.util.PreferenceUtil;

public class PageReaderBeeper extends BaseActivity {
    private LogList mLogList;

    private TextView mSet;

    private RadioGroup mGroupBeeper;

    private RFIDReaderHelper mReaderHelper;

    private static ReaderSetting m_curReaderSetting;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.page_reader_beeper);
        ((UHFApplication) getApplication()).addActivity(this);

        try {
            mReaderHelper = RFIDReaderHelper.getDefaultHelper();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        m_curReaderSetting = ReaderSetting.newInstance();

        mLogList = (LogList) findViewById(id.log_list);
        mSet = (TextView) findViewById(id.set);
        mGroupBeeper = (RadioGroup) findViewById(id.group_beeper);

        mSet.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                byte btMode = 0;
                BeeperUtils.BeepMode beepMode;
                switch (mGroupBeeper.getCheckedRadioButtonId()) {
                    case id.set_beeper_quiet:
                        btMode = 1;
                        beepMode = BeeperUtils.BeepMode.QUITE;
                        break;
                    case id.set_beeper_all:
                        btMode = 2;
                        beepMode = BeeperUtils.BeepMode.BEEP_INVENTORIED;
                        break;
                    case id.set_beeper_one:
                        btMode = 3;
                        beepMode = BeeperUtils.BeepMode.BEEP_PER_TAG;
                        break;
                    default:
                        beepMode = BeeperUtils.BeepMode.BEEP_INVENTORIED;
                        btMode = 0;
                        break;
                }
                PreferenceUtil.commitInt(BeeperUtils.BEEPER_MODEL, btMode);

                BeeperUtils.setBeepMode(beepMode);
                mLogList.writeLog(getApplication().getResources().getString(R.string.set_read_sound), 0x10);
            }
        });

        m_curReaderSetting.btBeeperMode =(byte)(PreferenceUtil.getInt(BeeperUtils.BEEPER_MODEL, 0)-1);
        updateView();
    }

    private void updateView() {
        if (m_curReaderSetting.btBeeperMode == 0) {
            mGroupBeeper.check(id.set_beeper_quiet);
        } else if (m_curReaderSetting.btBeeperMode == 1) {
            mGroupBeeper.check(id.set_beeper_all);
        } else if (m_curReaderSetting.btBeeperMode == +2) {
            mGroupBeeper.check(id.set_beeper_one);
        }
    }

    private RXObserver mObserver = new RXObserver() {
        @Override
        protected void onExeCMDStatus(final byte cmd, final byte status) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String strLog = FormatUtils.format(cmd, status);
                    mLogList.writeLog(strLog, status);
                }
            });
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        mReaderHelper.registerObserver(mObserver);
    }

    @Override
    protected void onResume() {
        mReaderHelper.startWith();
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mReaderHelper.unRegisterObserver(mObserver);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mLogList.tryClose()) return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}

