package com.udroid.uhf.activity.setpage;


import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.rfid.RFIDReaderHelper;
import com.rfid.config.CMD;
import com.rfid.rxobserver.RXObserver;
import com.rfid.rxobserver.ReaderSetting;
import com.udroid.uhf.R.id;
import com.udroid.uhf.R.layout;
import com.udroid.uhf.UHFApplication;
import com.udroid.uhf.activity.BaseActivity;
import com.udroid.uhf.widget.LogList;
import com.util.FormatUtils;

public class PageReaderFirmwareVersion extends BaseActivity {
    private LogList mLogList;

    private TextView mGet;

    private EditText mFirmwareVersionEditText;

    private RFIDReaderHelper mReaderHelper;

    private ReaderSetting m_curReaderSetting;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.page_reader_firmware_version);
        ((UHFApplication) getApplication()).addActivity(this);

        try {
            mReaderHelper = RFIDReaderHelper.getDefaultHelper();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        m_curReaderSetting = ReaderSetting.newInstance();

        mLogList = (LogList) findViewById(id.log_list);
        mGet = (TextView) findViewById(id.get);
        mFirmwareVersionEditText = (EditText) findViewById(id.firmware_version_text);

        mGet.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mReaderHelper.getFirmwareVersion(m_curReaderSetting.btReadId);
            }
        });

        updateView();
    }

    private void updateView() {
        mFirmwareVersionEditText.setText(String.valueOf(m_curReaderSetting.btMajor & 0xFF) + "." + String.valueOf(m_curReaderSetting.btMinor & 0xFF));
    }

    private RXObserver mObserver = new RXObserver() {
        @Override
        protected void onExeCMDStatus(final byte cmd, final byte status) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (cmd == CMD.GET_FIRMWARE_VERSION) {
                        updateView();
                    }
                    String strLog = FormatUtils.format(cmd, status);
                    mLogList.writeLog(strLog, status);
                }
            });
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        mReaderHelper.registerObserver(mObserver);
    }

    @Override
    protected void onResume() {
        mReaderHelper.startWith();
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mReaderHelper.unRegisterObserver(mObserver);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mLogList.tryClose()) return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}

