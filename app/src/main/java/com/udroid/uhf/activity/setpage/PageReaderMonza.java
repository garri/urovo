package com.udroid.uhf.activity.setpage;


import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.rfid.RFIDReaderHelper;
import com.rfid.config.CMD;
import com.rfid.rxobserver.RXObserver;
import com.rfid.rxobserver.ReaderSetting;
import com.udroid.uhf.R;
import com.udroid.uhf.UHFApplication;
import com.udroid.uhf.activity.BaseActivity;
import com.udroid.uhf.widget.LogList;
import com.util.FormatUtils;

public class PageReaderMonza extends BaseActivity {
	private LogList mLogList;
	
	private TextView mGet, mSet;

	private RadioGroup mGroupMonzaStatus, mGroupMonzaStore;

	private RFIDReaderHelper mReaderHelper;

	private static ReaderSetting m_curReaderSetting;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.page_reader_monza);
		((UHFApplication) getApplication()).addActivity(this);
		
		try {
			mReaderHelper = RFIDReaderHelper.getDefaultHelper();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		m_curReaderSetting = ReaderSetting.newInstance();

		mLogList = (LogList) findViewById(R.id.log_list);
		mGet = (TextView) findViewById(R.id.get);
		mSet = (TextView) findViewById(R.id.set);
		mSet.setOnClickListener(setMonzaClickListener);
		mGet.setOnClickListener(setMonzaClickListener);

		mGroupMonzaStatus = (RadioGroup) findViewById(R.id.group_monza_status);
		mGroupMonzaStore = (RadioGroup) findViewById(R.id.group_monza_store);

		updateView();
	}
	
	private void updateView() {
		if (m_curReaderSetting.btMonzaStatus == 0x00) {
			mGroupMonzaStatus.check(R.id.set_monza_close);
		} else if ((m_curReaderSetting.btMonzaStatus & 0xFF) == 0x8D) {
			mGroupMonzaStatus.check(R.id.set_monza_open);
		}
		
		if (m_curReaderSetting.blnMonzaStore) {
			mGroupMonzaStore.check(R.id.set_monza_save);
		} else {
			mGroupMonzaStore.check(R.id.set_monza_unsave);
		}
	}
	
	
	private OnClickListener setMonzaClickListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			switch(arg0.getId()) {
			case R.id.get:
				mReaderHelper.getImpinjFastTid(m_curReaderSetting.btReadId);
				break;
			case R.id.set:
				boolean blnOpen = (mGroupMonzaStatus.getCheckedRadioButtonId() == R.id.set_monza_open);
				boolean blnSave = (mGroupMonzaStore.getCheckedRadioButtonId() == R.id.set_monza_save);
				mReaderHelper.setImpinjFastTid(m_curReaderSetting.btReadId, blnOpen, blnSave);
				m_curReaderSetting.btMonzaStatus = (byte) (blnOpen ? 0x8D : 0x00);
				m_curReaderSetting.blnMonzaStore = blnSave;
				break;
			}
		}
	};

	private RXObserver mObserver = new RXObserver() {
		@Override
		protected void onExeCMDStatus(final byte cmd, final byte status) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (cmd == CMD.SET_AND_SAVE_IMPINJ_FAST_TID || cmd == CMD.GET_IMPINJ_FAST_TID) {
						updateView();
					}
					String strLog = FormatUtils.format(cmd, status);
					mLogList.writeLog(strLog, status);
				}
			});
		}

		@Override
		protected void refreshSetting(ReaderSetting readerSetting) {
			Log.v("ubx",readerSetting.blnMonzaStore ? "true":"false");
		}
	};

	@Override
	protected void onStart() {
		super.onStart();
		mReaderHelper.registerObserver(mObserver);
	}

	@Override
	protected void onResume() {
		mReaderHelper.startWith();
		super.onResume();
	}

	@Override
	protected void onStop() {
		super.onStop();
		mReaderHelper.unRegisterObserver(mObserver);
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (mLogList.tryClose()) return true;
		}

		return super.onKeyDown(keyCode, event);
	}
}

