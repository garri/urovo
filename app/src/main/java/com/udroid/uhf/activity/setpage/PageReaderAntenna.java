package com.udroid.uhf.activity.setpage;


import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TableRow;
import android.widget.TextView;

import com.rfid.RFIDReaderHelper;
import com.rfid.config.CMD;
import com.rfid.rxobserver.RXObserver;
import com.rfid.rxobserver.ReaderSetting;
import com.udroid.uhf.R;
import com.udroid.uhf.R.id;
import com.udroid.uhf.R.layout;
import com.udroid.uhf.UHFApplication;
import com.udroid.uhf.activity.BaseActivity;
import com.udroid.uhf.adapter.AbstractSpinerAdapter.IOnItemSelectListener;
import com.udroid.uhf.dialog.SpinerPopWindow;
import com.udroid.uhf.widget.LogList;
import com.util.FormatUtils;

import java.util.ArrayList;
import java.util.List;


public class PageReaderAntenna extends BaseActivity {
    private LogList mLogList;

    private TextView mSet;
    private TextView mGet;

    private TextView mAntennaTextView;
    private TableRow mDropDownRow;
    private List<String> mAntennaList = new ArrayList<String>();

    private SpinerPopWindow mSpinerPopWindow;

    private RFIDReaderHelper mReaderHelper;

    private int mPos = -1;

    private static ReaderSetting m_curReaderSetting;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.page_reader_antenna);
        ((UHFApplication) getApplication()).addActivity(this);

        try {
            mReaderHelper = RFIDReaderHelper.getDefaultHelper();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        m_curReaderSetting = ReaderSetting.newInstance();

        mLogList = (LogList) findViewById(id.log_list);
        mSet = (TextView) findViewById(id.set);
        mGet = (TextView) findViewById(id.get);
        mAntennaTextView = (TextView) findViewById(id.antenna_text);
        mDropDownRow = (TableRow) findViewById(id.table_row_spiner_antenna);

        mSet.setOnClickListener(setOutPowerOnClickListener);

        mGet.setOnClickListener(setOutPowerOnClickListener);

        mDropDownRow.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                showSpinWindow();
            }
        });


        String[] lists = getResources().getStringArray(R.array.antenna_list);
        for (int i = 0; i < lists.length; i++) {
            mAntennaList.add(lists[i]);
        }

        mSpinerPopWindow = new SpinerPopWindow(this);
        mSpinerPopWindow.refreshData(mAntennaList, 0);
        mSpinerPopWindow.setItemListener(new IOnItemSelectListener() {
            public void onItemClick(int pos) {
                setAntennaText(pos);
            }
        });

        updateView();
    }

    private void setAntennaText(int pos) {
        if (pos >= 0 && pos < mAntennaList.size()) {
            String value = mAntennaList.get(pos);
            mAntennaTextView.setText(value);
            mPos = pos;
        }
    }

    private void showSpinWindow() {
        mSpinerPopWindow.setWidth(mDropDownRow.getWidth());
        mSpinerPopWindow.showAsDropDown(mDropDownRow);
    }

    private void updateView() {

        mPos = m_curReaderSetting.btWorkAntenna;

        if (mPos >= 0 && mPos < mAntennaList.size())
            mAntennaTextView.setText(mAntennaList.get(mPos));
    }

    private OnClickListener setOutPowerOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            switch (arg0.getId()) {
                case id.get:
                    mReaderHelper.getWorkAntenna(m_curReaderSetting.btReadId);
                    break;
                case id.set:
                    byte btWorkAntenna = (byte) mPos;
                    if (btWorkAntenna < 0 || btWorkAntenna > mAntennaList.size()) return;

                    mReaderHelper.setWorkAntenna(m_curReaderSetting.btReadId, btWorkAntenna);
                    m_curReaderSetting.btWorkAntenna = btWorkAntenna;
                    break;
            }
        }
    };

    private RXObserver mObserver = new RXObserver() {
        @Override
        protected void onExeCMDStatus(final byte cmd, final byte status) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (cmd == CMD.GET_WORK_ANTENNA || cmd == CMD.SET_WORK_ANTENNA) {
                        updateView();
                    }
                    String strLog = FormatUtils.format(cmd, status);
                    mLogList.writeLog(strLog, status);
                }
            });
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        mReaderHelper.registerObserver(mObserver);
    }

    @Override
    protected void onResume() {
        mReaderHelper.startWith();
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mReaderHelper.unRegisterObserver(mObserver);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mLogList.tryClose()) return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}

