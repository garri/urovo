package com.udroid.uhf.activity.setpage;


import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.rfid.RFIDReaderHelper;
import com.rfid.config.CMD;
import com.rfid.rxobserver.RXObserver;
import com.rfid.rxobserver.ReaderSetting;
import com.udroid.uhf.R.id;
import com.udroid.uhf.R.layout;
import com.udroid.uhf.UHFApplication;
import com.udroid.uhf.activity.BaseActivity;
import com.udroid.uhf.widget.LogList;
import com.util.FormatUtils;
import com.util.StringTool;

public class PageReaderIdentifier extends BaseActivity {
	private LogList mLogList;
	
	private TextView mSet;
	private TextView mGet;
	
	private EditText mSetIdentifierText;
	private EditText mGetIdentifierText;
	
	private RFIDReaderHelper mReaderHelper;

	private static ReaderSetting m_curReaderSetting;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(layout.page_reader_identifier);
		((UHFApplication) getApplication()).addActivity(this);
		
		try {
			mReaderHelper = RFIDReaderHelper.getDefaultHelper();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		m_curReaderSetting = ReaderSetting.newInstance();

		mLogList = (LogList) findViewById(id.log_list);
		mSet = (TextView) findViewById(id.set);
		mGet = (TextView) findViewById(id.get);
		mSetIdentifierText = (EditText) findViewById(id.set_identifier_text);
		mGetIdentifierText = (EditText) findViewById(id.get_identifier_text);
		
		mSet.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String strIdentifier = mSetIdentifierText.getText().toString();
				
				if (strIdentifier != null) strIdentifier = strIdentifier.trim();
				
				if (strIdentifier == null || strIdentifier.equals(""))	return ;
				
				String []strAryIdentifier = StringTool.stringToStringArray(strIdentifier, 2);
				byte[] btAryIdentifier = StringTool.stringArrayToByteArray(strAryIdentifier, 12);
				if (btAryIdentifier == null) return;
				
				m_curReaderSetting.btAryReaderIdentifier = btAryIdentifier;
				mReaderHelper.setReaderIdentifier(m_curReaderSetting.btReadId, btAryIdentifier);
			}
		});
		
		mGet.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				mReaderHelper.getReaderIdentifier(m_curReaderSetting.btReadId);
			}
		});
		
		if (m_curReaderSetting.btAryReaderIdentifier != null) {
			String strIdentifier = String.format("%02X", m_curReaderSetting.btAryReaderIdentifier[0]);
	        for (int i = 1; i < 12; i ++) {
	        	strIdentifier = strIdentifier + " " + String.format("%02X", m_curReaderSetting.btAryReaderIdentifier[i]);
	        }
			mGetIdentifierText.setText(strIdentifier.toUpperCase());
		}
	}

	private RXObserver mObserver = new RXObserver() {
		@Override
		protected void onExeCMDStatus(final byte cmd, final byte status) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (cmd == CMD.GET_READER_IDENTIFIER || cmd == CMD.SET_READER_IDENTIFIER) {
						if (m_curReaderSetting.btAryReaderIdentifier != null) {
							String strIdentifier = String.format("%02X", m_curReaderSetting.btAryReaderIdentifier[0]);
							for (int i = 1; i < 12; i ++) {
								strIdentifier = strIdentifier + " " + String.format("%02X", m_curReaderSetting.btAryReaderIdentifier[i]);
							}
							mGetIdentifierText.setText(strIdentifier.toUpperCase());
						}
					}
					String strLog = FormatUtils.format(cmd, status);
					mLogList.writeLog(strLog, status);
				}
			});
		}
	};

	@Override
	protected void onStart() {
		super.onStart();
		mReaderHelper.registerObserver(mObserver);
	}

	@Override
	protected void onResume() {
		mReaderHelper.startWith();
		super.onResume();
	}

	@Override
	protected void onStop() {
		super.onStop();
		mReaderHelper.unRegisterObserver(mObserver);
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (mLogList.tryClose()) return true;
		}

		return super.onKeyDown(keyCode, event);
	}
}

