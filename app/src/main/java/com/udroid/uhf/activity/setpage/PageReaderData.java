package com.udroid.uhf.activity.setpage;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.udroid.uhf.R;
import com.udroid.uhf.activity.BaseActivity;
import com.util.PreferenceUtil;

/**
 * create by 大白菜
 * description
 */
public class PageReaderData extends BaseActivity {

    private RadioGroup group;
    private RadioButton rbClose, rbOpen;
    private TextView btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_reader_data);
        initView();
    }

    private void initView() {
        group = (RadioGroup) findViewById(R.id.group_data);
        if (PreferenceUtil.getBoolean("data_open",false)){
            group.check(R.id.rb_open);
        } else {
            group.check(R.id.rb_close);
        }
//        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup radioGroup, int i) {
//            }
//        });

        btnSave = (TextView) findViewById(R.id.set);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (group.getCheckedRadioButtonId() == R.id.rb_open){
                     PreferenceUtil.commitBoolean("data_open",true);
                } else {
                    PreferenceUtil.commitBoolean("data_open",false);
                }
            }
        });
    }
}
