package com.udroid.uhf.activity.setpage;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.rfid.RFIDReaderHelper;
import com.rfid.bean.MessageTran;
import com.rfid.config.CMD;
import com.rfid.rxobserver.RXObserver;
import com.rfid.rxobserver.ReaderSetting;
import com.udroid.uhf.R;
import com.udroid.uhf.UHFApplication;
import com.udroid.uhf.activity.BaseActivity;
import com.udroid.uhf.widget.LogList;
import com.util.FormatUtils;

/**
 * Created by Administrator on 2018/4/28.
 */

public class PageReaderStatus extends BaseActivity {
    private LogList mLogList;

    private TextView mGet;
    private TextView mReset;

    private EditText mReaderStatusInfoEdit;

    private RFIDReaderHelper mReaderHelper;

    private static ReaderSetting m_curReaderSetting;
    private byte mReaderStatus = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_reader_status);
        ((UHFApplication) getApplication()).addActivity(this);

        try {
            mReaderHelper = RFIDReaderHelper.getDefaultHelper();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        m_curReaderSetting = ReaderSetting.newInstance();

        mLogList = (LogList) findViewById(R.id.log_list);
        mGet = (TextView) findViewById(R.id.get);
        mReaderStatusInfoEdit = (EditText) findViewById(R.id.reader_status_info);
        mReset = (TextView) findViewById(R.id.resort_normal);

        mGet.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mReaderHelper.sendCommand(new byte[]{(byte) 0xA0, (byte) 0x03, (byte) 0xFF, (byte) 0xA1,
                        (byte) 0xBD});
            }
        });

        mReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mReaderStatus = (byte) Integer.parseInt(mReaderStatusInfoEdit.getText().toString(), 16);
                    MessageTran messageTran = new MessageTran(m_curReaderSetting.btReadId, (byte) 0xA0, new byte[]{mReaderStatus});
                    mReaderHelper.sendCommand(messageTran.getAryTranData());
                } catch (Exception ex) {
                    Toast.makeText(UHFApplication.getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        updateView();
    }

    private void updateView() {
        String[] strs = mLogList.getTitleInfo().toString().split("  ");
        if (mReaderStatus == 0x13) {
            mLogList.setTitleInfo(strs[0] + "  ");
        } else if (mReaderStatus == -1) {

        } else {
            if (!strs[strs.length - 1].equals("<" +
                    getResources().getString(R.string.auto_read_model) + ">")) {
                SpannableString tSS = new SpannableString(strs[0] + "  " + "  <" +
                        getResources().getString(R.string.auto_read_model) + ">");
                tSS.setSpan(new ForegroundColorSpan(Color.RED), 0, tSS.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                mLogList.setTitleInfo(tSS);
            }
        }
        mReaderStatusInfoEdit.setText(String.format("%02x", mReaderStatus));
    }

    private RXObserver mObserver = new RXObserver() {
        @Override
        protected void onExeCMDStatus(final byte cmd, final byte status) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (cmd == CMD.QUERY_READER_STATUS || cmd == CMD.SET_READER_STATUS) {
                        mReaderStatusInfoEdit.setText(String.format("%02x", mReaderStatus));
                        //updateView();
                    }
                    String strLog = FormatUtils.format(cmd, status);
                    mLogList.writeLog(strLog, status);
                }
            });
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        mReaderHelper.registerObserver(mObserver);
    }

    @Override
    protected void onResume() {
        mReaderHelper.startWith();
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mReaderHelper.unRegisterObserver(mObserver);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mLogList.tryClose()) return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
