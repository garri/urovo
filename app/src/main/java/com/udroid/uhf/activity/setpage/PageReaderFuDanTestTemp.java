package com.udroid.uhf.activity.setpage;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.rfid.RFIDReaderHelper;
import com.udroid.uhf.R;
import com.udroid.uhf.UHFApplication;
import com.udroid.uhf.activity.BaseActivity;
import com.udroid.uhf.fragment.FragmentMessageListener;
import com.udroid.uhf.fragment.PageReaderFuDanCmdFragment;
import com.udroid.uhf.fragment.TestTempInventoryFragment;

import java.util.ArrayList;
import java.util.List;

public class PageReaderFuDanTestTemp extends BaseActivity implements FragmentMessageListener {

    private RFIDReaderHelper mReaderHelper;

    private ViewPager viewPager;
    private List<Fragment> fragments = new ArrayList<>();

    private TextView[] title = new TextView[2];
    private int currIndex = 0;

    public String epc = "";
    private boolean isClearMask = false;

    PageReaderFuDanCmdFragment fuDanCmdFragment ;
    TestTempInventoryFragment tempInventoryFragment ;

    private final String TAG = this.getClass().getSimpleName();

    @Override
    protected void onResume() {
        mReaderHelper.startWith();
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mReaderHelper != null) {
            mReaderHelper.clearTagMask((byte)0xFF,(byte)0x00);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_fudan_test_temperature);
        ((UHFApplication) getApplication()).addActivity(this);
        try {
            mReaderHelper = RFIDReaderHelper.getDefaultHelper();
        } catch (Exception e) {
            e.printStackTrace();
        }

        initView();
    }

    private void initView() {
        viewPager = findViewById(R.id.vPager);
        viewPager.setCurrentItem(0);

        fuDanCmdFragment = new PageReaderFuDanCmdFragment();
        tempInventoryFragment = new TestTempInventoryFragment();
        tempInventoryFragment.setFragmentMessageListener(this);
        fragments.add(tempInventoryFragment);
        fragments.add(fuDanCmdFragment);
        viewPager.setAdapter(new MyViewPagerAdapter(getSupportFragmentManager(),fragments));
        viewPager.setOnPageChangeListener(new MyViewPagerChang());

        title[0] = findViewById(R.id.tab_index1);
        title[1] = findViewById(R.id.tab_index2);

        title[0].setOnClickListener(new MyOnClickListener(0));
        title[1].setOnClickListener(new MyOnClickListener(1));
    }

    class MyViewPagerAdapter extends FragmentStatePagerAdapter {
        private List<Fragment> fragments;
        private FragmentManager fragmentManager;

        MyViewPagerAdapter(FragmentManager fragmentManager,List<Fragment> fragments) {
            super(fragmentManager);
            this.fragmentManager = fragmentManager;
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

    }

    class MyViewPagerChang implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            //fuDanCmdFragment.setEpc("");
        }

        @Override
        public void onPageSelected(int position) {
            currIndex = position;
            if (0 == currIndex) {
                title[1].setBackgroundResource(R.drawable.btn_select_background_select_left_down);
                title[0].setBackgroundResource(R.drawable.btn_select_background_select_right);
            } else {
                title[1].setBackgroundResource(R.drawable.btn_select_background_select_left);
                title[0].setBackgroundResource(R.drawable.btn_select_background_select_right_down);
            }
            title[1 - currIndex].setTextColor(Color.rgb(0x00, 0xBB, 0xF7));
            title[currIndex].setTextColor(Color.rgb(0xFF, 0xFF, 0xFF));
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    public class MyOnClickListener implements View.OnClickListener {
        private int index = 0;
        public MyOnClickListener(int i) {
            index = i;
        }
        @Override
        public void onClick(View v) {
            if (!isClearMask) {
                isClearMask = true;
                mReaderHelper.clearTagMask((byte)0xFF,(byte)0x00);
                fuDanCmdFragment.setEpc("");
            }
            viewPager.setCurrentItem(index);
        }
    }

    @Override
    public void sendMessage(String message) {
        if (isClearMask) {
            isClearMask = false;
            return;
        }
        epc = message;
        Log.d(TAG,"EPC:" + epc);
        viewPager.setCurrentItem(1);
        fuDanCmdFragment.setEpc(epc);
    }
}
