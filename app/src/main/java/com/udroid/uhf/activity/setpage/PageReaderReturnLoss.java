package com.udroid.uhf.activity.setpage;


import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;

import com.rfid.RFIDReaderHelper;
import com.rfid.config.CMD;
import com.rfid.rxobserver.RXObserver;
import com.rfid.rxobserver.ReaderSetting;
import com.udroid.uhf.R;
import com.udroid.uhf.UHFApplication;
import com.udroid.uhf.activity.BaseActivity;
import com.udroid.uhf.adapter.AbstractSpinerAdapter.IOnItemSelectListener;
import com.udroid.uhf.dialog.SpinerPopWindow;
import com.udroid.uhf.widget.LogList;
import com.util.FormatUtils;

import java.util.ArrayList;
import java.util.List;


public class PageReaderReturnLoss extends BaseActivity {
    private LogList mLogList;

    private TextView mMeasure;

    private EditText mReturnLossText;
    private TextView mMeasureText;
    private TableRow mDropDownRow;
    private List<String> mMeasureList = new ArrayList<String>();

    private SpinerPopWindow mSpinerPopWindow;

    private RFIDReaderHelper mReaderHelper;

    private int mPos = -1;

    private static ReaderSetting m_curReaderSetting;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_reader_return_loss);
        ((UHFApplication) getApplication()).addActivity(this);

        try {
            mReaderHelper = RFIDReaderHelper.getDefaultHelper();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        m_curReaderSetting = ReaderSetting.newInstance();

        mLogList = (LogList) findViewById(R.id.log_list);
        mMeasure = (TextView) findViewById(R.id.measure);
        mReturnLossText = (EditText) findViewById(R.id.return_loss_text);
        mMeasureText = (TextView) findViewById(R.id.measure_text);
        mDropDownRow = (TableRow) findViewById(R.id.table_row_spiner_return_loss);

        mMeasure.setOnClickListener(setReturnLossOnClickListener);

        mDropDownRow.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                showSpinWindow();
            }
        });


        String[] lists = getResources().getStringArray(R.array.return_loss_list);
        for (int i = 0; i < lists.length; i++) {
            mMeasureList.add(lists[i]);
        }

        mSpinerPopWindow = new SpinerPopWindow(this);
        mSpinerPopWindow.refreshData(mMeasureList, 0);
        mSpinerPopWindow.setItemListener(new IOnItemSelectListener() {
            public void onItemClick(int pos) {
                setMeasureText(pos);
            }
        });

        updateView();
    }

    private void setMeasureText(int pos) {
        if (pos >= 0 && pos < mMeasureList.size()) {
            String value = mMeasureList.get(pos);
            mMeasureText.setText(value);
            mPos = pos;
        }
    }

    private void showSpinWindow() {
        mSpinerPopWindow.setWidth(mDropDownRow.getWidth());
        mSpinerPopWindow.showAsDropDown(mDropDownRow);
    }

    private void updateView() {

        //add by lei.li 2016/11/24
        //mPos = m_curReaderSetting.btReturnLoss;
        //add by lei.li 2016/11/24
        mReturnLossText.setText(String.valueOf(m_curReaderSetting.btReturnLoss & 0xFF));
    }

    private OnClickListener setReturnLossOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            switch (arg0.getId()) {
                case R.id.measure:
                    byte btFrequency = (byte) mPos;
                    if (btFrequency < 0 || btFrequency > mMeasureList.size()) return;

                    mReaderHelper.getRfPortReturnLoss(m_curReaderSetting.btReadId, btFrequency);
                    m_curReaderSetting.btReturnLoss = btFrequency;
                    break;
            }
        }
    };

    private RXObserver mObserver = new RXObserver() {
        @Override
        protected void onExeCMDStatus(final byte cmd, final byte status) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (cmd == CMD.GET_RF_PORT_RETURN_LOSS) {
                        updateView();
                    }
                    String strLog = FormatUtils.format(cmd, status);
                    mLogList.writeLog(strLog, status);
                }
            });
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        mReaderHelper.registerObserver(mObserver);
    }

    @Override
    protected void onResume() {
        mReaderHelper.startWith();
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mReaderHelper.unRegisterObserver(mObserver);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mLogList.tryClose()) return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}

