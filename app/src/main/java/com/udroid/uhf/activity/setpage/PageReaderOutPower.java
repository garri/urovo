package com.udroid.uhf.activity.setpage;


import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.rfid.RFIDReaderHelper;
import com.rfid.config.CMD;
import com.rfid.rxobserver.RXObserver;
import com.rfid.rxobserver.ReaderSetting;
import com.udroid.uhf.R;
import com.udroid.uhf.R.id;
import com.udroid.uhf.R.layout;
import com.udroid.uhf.UHFApplication;
import com.udroid.uhf.activity.BaseActivity;
import com.udroid.uhf.widget.LogList;
import com.util.FormatUtils;

public class PageReaderOutPower extends BaseActivity {
    private LogList mLogList;

    private TextView mSet;
    private TextView mGet;

    private EditText mOutPowerText;

    private RFIDReaderHelper mReaderHelper;

    private static ReaderSetting m_curReaderSetting;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_reader_out_power);
        ((UHFApplication) getApplication()).addActivity(this);

        try {
            mReaderHelper = RFIDReaderHelper.getDefaultHelper();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        m_curReaderSetting = ReaderSetting.newInstance();

        mLogList = (LogList) findViewById(id.log_list);
        mSet = (TextView) findViewById(id.set);
        mGet = (TextView) findViewById(id.get);
        mOutPowerText = (EditText) findViewById(id.out_power_text);

        mSet.setOnClickListener(setOutPowerOnClickListener);

        mGet.setOnClickListener(setOutPowerOnClickListener);

        updateView();
    }

    private void updateView() {

        if (m_curReaderSetting.btAryOutputPower != null) {
            mOutPowerText.setText(String.valueOf(m_curReaderSetting.btAryOutputPower[0] & 0xFF));
        }
    }

    private OnClickListener setOutPowerOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            switch (arg0.getId()) {
                case id.get:
                    Log.v("ubx","get power");
                    mReaderHelper.getOutputPower(m_curReaderSetting.btReadId);
                    break;
                case id.set:
                    byte btOutputPower = 0x00;
                    try {
                        btOutputPower = (byte) Integer.parseInt(mOutPowerText.getText().toString());
                    } catch (Exception e) {
                        ;
                    }
                    mReaderHelper.setOutputPower(m_curReaderSetting.btReadId, btOutputPower);
                    m_curReaderSetting.btAryOutputPower = new byte[]{btOutputPower};
                    break;
            }
        }
    };

    private RXObserver mObserver = new RXObserver() {
        @Override
        protected void onExeCMDStatus(final byte cmd, final byte status) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (cmd == CMD.GET_OUTPUT_POWER || cmd == CMD.SET_OUTPUT_POWER) {
                        updateView();
                    }
                    String strLog = FormatUtils.format(cmd, status);
                    mLogList.writeLog(strLog, status);
                }
            });
        }

        @Override
        protected void refreshSetting(ReaderSetting readerSetting) {
            super.refreshSetting(readerSetting);
            Log.v("ubx",String.valueOf(readerSetting.btAryOutputPower[0] & 0xFF));
        }
        
    };

    @Override
    protected void onStart() {
        super.onStart();
        mReaderHelper.registerObserver(mObserver);
    }

    @Override
    protected void onResume() {
        mReaderHelper.startWith();
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mReaderHelper.unRegisterObserver(mObserver);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mLogList.tryClose()) return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}

