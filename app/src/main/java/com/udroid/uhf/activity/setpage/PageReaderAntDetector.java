package com.udroid.uhf.activity.setpage;


import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.rfid.RFIDReaderHelper;
import com.rfid.config.CMD;
import com.rfid.rxobserver.RXObserver;
import com.rfid.rxobserver.ReaderSetting;
import com.udroid.uhf.R;
import com.udroid.uhf.UHFApplication;
import com.udroid.uhf.activity.BaseActivity;
import com.udroid.uhf.widget.LogList;
import com.util.FormatUtils;


public class PageReaderAntDetector extends BaseActivity {
	private LogList mLogList;
	
	private TextView mSet;
	private TextView mGet;
	
	private EditText mAntDetectorText;
	
	private RFIDReaderHelper mReaderHelper;

	private static ReaderSetting m_curReaderSetting;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.page_reader_ant_detector);
		((UHFApplication) getApplication()).addActivity(this);
		
		try {
			mReaderHelper = RFIDReaderHelper.getDefaultHelper();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		m_curReaderSetting = ReaderSetting.newInstance();

		mLogList = (LogList) findViewById(R.id.log_list);
		mSet = (TextView) findViewById(R.id.set);
		mGet = (TextView) findViewById(R.id.get);
		mAntDetectorText = (EditText) findViewById(R.id.ant_detector_text);
		
		mSet.setOnClickListener(setAntDetectorOnClickListener);
		mGet.setOnClickListener(setAntDetectorOnClickListener);
		
		updateView();
	}
	
	private void updateView() {
        mAntDetectorText.setText(String.valueOf(m_curReaderSetting.btAntDetector & 0xFF));
	}
	
	private OnClickListener setAntDetectorOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			switch(arg0.getId()) {
			case R.id.get:
				mReaderHelper.getAntConnectionDetector(m_curReaderSetting.btReadId);
				break;
			case R.id.set:
				byte btDetectorStatus = 0x00;
				try {
					btDetectorStatus = (byte)Integer.parseInt(mAntDetectorText.getText().toString());
				} catch (Exception e) {
					Toast.makeText(PageReaderAntDetector.this,"Invaild number!",Toast.LENGTH_LONG).show();
					return;
				}

				mReaderHelper.setAntConnectionDetector(m_curReaderSetting.btReadId, btDetectorStatus);
				m_curReaderSetting.btAntDetector = btDetectorStatus;
				break;
			}
		}
	};

	private RXObserver mObserver = new RXObserver() {
		@Override
		protected void onExeCMDStatus(final byte cmd, final byte status) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (cmd == CMD.GET_ANT_CONNECTION_DETECTOR || cmd == CMD.SET_ANT_CONNECTION_DETECTOR) {
						updateView();
					}
					String strLog = FormatUtils.format(cmd, status);
					mLogList.writeLog(strLog, status);
				}
			});
		}
	};

	@Override
	protected void onStart() {
		super.onStart();
		mReaderHelper.registerObserver(mObserver);
	}

	@Override
	protected void onResume() {
		mReaderHelper.startWith();
		super.onResume();
	}

	@Override
	protected void onStop() {
		super.onStop();
		mReaderHelper.unRegisterObserver(mObserver);
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (mLogList.tryClose()) return true;
		}

		return super.onKeyDown(keyCode, event);
	}
}

