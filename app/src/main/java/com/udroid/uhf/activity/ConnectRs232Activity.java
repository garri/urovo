package com.udroid.uhf.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.module.interaction.ModuleConnector;
import com.module.interaction.ReaderHelper;
import com.rfid.ReaderConnector;
import com.udroid.uhf.R;
import com.udroid.uhf.UHFApplication;
import com.udroid.uhf.dialog.SpinerPopWindow;
import com.util.OtgUtils;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

@SuppressLint("HandlerLeak")
/**
 * @author Administrator
 *
 */
public class ConnectRs232Activity extends BaseActivity {

    //add by lei.li 2016//11/14
    private static final String TAG = "COONECTRS232";
    private static final String TTYS1 = "ttyHLS0 (rk_serial)";
    private static final boolean DEBUG = true;
    private ReaderHelper mReaderHelper;
    private List<String> mPortList = new ArrayList<String>();
    private TextView mPortTextView, mBaud115200View, mBaud38400View;
    private TableRow mDropPort;
    private SpinerPopWindow mSpinerPort;
    private Context mcontext;
    private int mPosPort = -1;
    public static ModuleConnector connector = new ReaderConnector();

    String[] entries = null;
    String[] entryValues = null;

    private int baud = 115200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connect_rs232);
        ((UHFApplication) getApplication()).addActivity(this);

        mcontext = this;
        //add by lei.li 2016//11/14
        TextView connectButton = (TextView) findViewById(R.id.textview_connect);

        mPortTextView = (TextView) findViewById(R.id.comport_text);
        mBaud115200View = (TextView) findViewById(R.id.baud_115200);
        mBaud38400View = (TextView) findViewById(R.id.baud_38400);
        mDropPort = (TableRow) findViewById(R.id.table_row_spiner_comport);

        //add by lei.li 2016/11/14 set default serial port
        mPortTextView.setText(TTYS1);
        //add by lei.li 2016/11/14

        baud = 115200;
//		mBaud115200View.setBackgroundColor(Color.rgb(0xFF, 0xFF, 0xFF));
//		mBaud38400View.setBackgroundColor(Color.rgb(0xCC, 0xCC, 0xCC));

        mBaud115200View.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//				mBaud115200View.setBackgroundColor(Color.rgb(0xFF, 0xFF, 0xFF));
//				mBaud38400View.setBackgroundColor(Color.rgb(0xCC, 0xCC, 0xCC));
                baud = 115200;
            }
        });

        mBaud38400View.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mBaud38400View.setBackgroundColor(Color.rgb(0xFF, 0xFF, 0xFF));
                mBaud115200View.setBackgroundColor(Color.rgb(0xCC, 0xCC, 0xCC));
                baud = 38400;
            }
        });

        connectButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (mPortList.indexOf(mPortTextView.getText().toString()) >= 0)
                    mPosPort = mPortList.indexOf(mPortTextView.getText().toString());

                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (DEBUG)
                    Log.e(TAG, "test the value of mPosPort::" + mPosPort);
                try {
                    OtgUtils.set53CGPIOEnabled(true);
                    SystemClock.sleep(150);
                    if (connector.connectCom("/dev/ttyHSL0", baud, mcontext)) { //sq53c:/dev/ttyHSL0  //sq53:/dev/ttyMSM1  //sq52:dev/ttyUSB0
                        Log.e(TAG, "connectCom: success");
                        Intent intent;
                        intent = new Intent().setClass(ConnectRs232Activity.this, MainActivity.class);
                        startActivity(intent);
                    }
                } catch (SecurityException e) {
                    Toast.makeText(
                            getApplicationContext(),
                            getResources().getString(R.string.error_security),
                            Toast.LENGTH_SHORT).show();
                } catch (InvalidParameterException e) {
                    Toast.makeText(
                            getApplicationContext(),
                            getResources().getString(R.string.error_configuration),
                            Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

       /* mDropPort.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                showPortSpinWindow();
            }
        });

        String[] lists = entries;
        for (int i = 0; i < lists.length; i++) {
            mPortList.add(lists[i]);
        }

        mSpinerPort = new SpinerPopWindow(this);
        mSpinerPort.refreshData(mPortList, 0);
        mSpinerPort.setItemListener(new IOnItemSelectListener() {
            public void onItemClick(int pos) {
                setPortText(pos);
            }
        });
*/
    }

    private void showPortSpinWindow() {
        mSpinerPort.setWidth(mDropPort.getWidth());
        mSpinerPort.showAsDropDown(mDropPort);
    }

    private void setPortText(int pos) {
        if (pos >= 0 && pos < mPortList.size()) {
            String value = mPortList.get(pos);
            mPortTextView.setText(value);
            mPosPort = pos;

        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            OtgUtils.set53CGPIOEnabled(false);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        OtgUtils.set53CGPIOEnabled(true);
        registerReceiver(mReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    @Override
    protected void onDestroy() {
        OtgUtils.set53CGPIOEnabled(false);
        super.onDestroy();
    }

    /**
     * 电量低警告
     */
    private AlertDialog mWornDialog = null;
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //获取当前电量,范围是 0～100
            int level = intent.getIntExtra("level", 0);
            if (level < 10) {
                if (mWornDialog == null) {
                    mWornDialog = new AlertDialog.Builder(ConnectRs232Activity.this)
                            .setCancelable(false)
                            .setTitle("警告")
                            .setMessage("当前电量低于10%")
                            .setNegativeButton("确定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).show();

                } else if (!mWornDialog.isShowing()) {
                    mWornDialog.show();
                }
            }
        }
    };


}
