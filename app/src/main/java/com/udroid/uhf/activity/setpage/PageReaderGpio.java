package com.udroid.uhf.activity.setpage;


import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.rfid.RFIDReaderHelper;
import com.rfid.config.CMD;
import com.rfid.rxobserver.RXObserver;
import com.rfid.rxobserver.ReaderSetting;
import com.udroid.uhf.R.id;
import com.udroid.uhf.R.layout;
import com.udroid.uhf.UHFApplication;
import com.udroid.uhf.activity.BaseActivity;
import com.udroid.uhf.widget.LogList;
import com.util.FormatUtils;

public class PageReaderGpio extends BaseActivity {
	private LogList mLogList;
	
	private TextView mGetGpio;
	private TextView mSetGpio3, mSetGpio4;
	
	private RadioGroup mGroupGpio1, mGroupGpio2, mGroupGpio3, mGroupGpio4;

	private RFIDReaderHelper mReaderHelper;

	private static ReaderSetting m_curReaderSetting;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(layout.page_reader_gpio);
		((UHFApplication) getApplication()).addActivity(this);
		
		try {
			mReaderHelper = RFIDReaderHelper.getDefaultHelper();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		m_curReaderSetting = ReaderSetting.newInstance();

		mLogList = (LogList) findViewById(id.log_list);
		mGetGpio = (TextView) findViewById(id.get_gpio);
		mGetGpio.setOnClickListener(setGpioOnClickListener);
		mSetGpio3 = (TextView) findViewById(id.set_gpio3);
		mSetGpio3.setOnClickListener(setGpioOnClickListener);
		mSetGpio4 = (TextView) findViewById(id.set_gpio4);
		mSetGpio4.setOnClickListener(setGpioOnClickListener);
		
		mGroupGpio1 = (RadioGroup) findViewById(id.group_gpio1);
		mGroupGpio2 = (RadioGroup) findViewById(id.group_gpio2);
		mGroupGpio3 = (RadioGroup) findViewById(id.group_gpio3);
		mGroupGpio4 = (RadioGroup) findViewById(id.group_gpio4);
		
		updateView();
	}
	
	private void updateView() {
		if (m_curReaderSetting.btGpio1Value == 0x00) {
			mGroupGpio1.check(id.get_gpio1_0);
		} else if (m_curReaderSetting.btGpio1Value == 0x01) {
			mGroupGpio1.check(id.get_gpio1_1);
		}
		
		if (m_curReaderSetting.btGpio2Value == 0x00) {
			mGroupGpio2.check(id.get_gpio2_0);
		} else if (m_curReaderSetting.btGpio2Value == 0x01) {
			mGroupGpio2.check(id.get_gpio2_1);
		}
		
		if (m_curReaderSetting.btGpio3Value == 0x00) {
			mGroupGpio3.check(id.set_gpio3_0);
		} else if (m_curReaderSetting.btGpio3Value == 0x01) {
			mGroupGpio3.check(id.set_gpio3_1);
		}
		
		if (m_curReaderSetting.btGpio4Value == 0x00) {
			mGroupGpio4.check(id.set_gpio4_0);
		} else if (m_curReaderSetting.btGpio4Value == 0x01) {
			mGroupGpio4.check(id.set_gpio4_1);
		}
	}
	
	
	private OnClickListener setGpioOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			switch(arg0.getId()) {
			case id.get_gpio:
				mReaderHelper.readGpioValue(m_curReaderSetting.btReadId);
				break;
			case id.set_gpio3:
				byte btGpio3Value = (byte) (mGroupGpio3.getCheckedRadioButtonId() == id.set_gpio3_0 ? 0x00 : 0x01);
				mReaderHelper.writeGpioValue(m_curReaderSetting.btReadId, (byte)0x03, btGpio3Value);
				m_curReaderSetting.btGpio3Value = btGpio3Value;
				break;
			case id.set_gpio4:
				byte btGpio4Value = (byte) (mGroupGpio3.getCheckedRadioButtonId() == id.set_gpio4_0 ? 0x00 : 0x01);
				mReaderHelper.writeGpioValue(m_curReaderSetting.btReadId, (byte)0x04, btGpio4Value);
				m_curReaderSetting.btGpio4Value = btGpio4Value;
				break;
			}
		}
	};

	private RXObserver mObserver = new RXObserver() {
		@Override
		protected void onExeCMDStatus(final byte cmd, final byte status) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (cmd == CMD.READ_GPIO_VALUE || cmd == CMD.WRITE_GPIO_VALUE) {
						updateView();
					}
					String strLog = FormatUtils.format(cmd, status);
					mLogList.writeLog(strLog, status);
				}
			});
		}
	};

	@Override
	protected void onStart() {
		super.onStart();
		mReaderHelper.registerObserver(mObserver);
	}

	protected void onResume() {
		mReaderHelper.startWith();
		super.onResume();
	};

	@Override
	protected void onStop() {
		super.onStop();
		mReaderHelper.unRegisterObserver(mObserver);
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (mLogList.tryClose()) return true;
		}

		return super.onKeyDown(keyCode, event);
	}
}

