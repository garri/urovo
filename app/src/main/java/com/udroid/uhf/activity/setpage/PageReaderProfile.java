package com.udroid.uhf.activity.setpage;


import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.rfid.RFIDReaderHelper;
import com.rfid.config.CMD;
import com.rfid.rxobserver.RXObserver;
import com.rfid.rxobserver.ReaderSetting;
import com.udroid.uhf.R.id;
import com.udroid.uhf.R.layout;
import com.udroid.uhf.UHFApplication;
import com.udroid.uhf.activity.BaseActivity;
import com.udroid.uhf.widget.LogList;
import com.util.FormatUtils;

public class PageReaderProfile extends BaseActivity {
	private LogList mLogList;
	
	private TextView mSet, mGet;
	
	private RadioGroup mGroupProfile;
	
	private RFIDReaderHelper mReaderHelper;

	private static ReaderSetting m_curReaderSetting;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(layout.page_reader_profile);
		((UHFApplication) getApplication()).addActivity(this);
		
		try {
			mReaderHelper = RFIDReaderHelper.getDefaultHelper();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		m_curReaderSetting = ReaderSetting.newInstance();

		mLogList = (LogList) findViewById(id.log_list);
		mSet = (TextView) findViewById(id.set);
		mGet = (TextView) findViewById(id.get);
		mGroupProfile =  (RadioGroup) findViewById(id.group_profile);
		
		mSet.setOnClickListener(setProfileOnClickListener);
		mGet.setOnClickListener(setProfileOnClickListener);
		
		updateView();
	}
	
	private OnClickListener setProfileOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			switch(arg0.getId()) {
			case id.get:
				mReaderHelper.getRfLinkProfile(m_curReaderSetting.btReadId);
				break;
			case id.set:
				byte btProfile = 0;
				switch (mGroupProfile.getCheckedRadioButtonId()) {
				case id.set_profile_option0:
					btProfile = (byte) 0xD0;
					break;
				case id.set_profile_option1:
					btProfile = (byte) 0xD1;
					break;
				case id.set_profile_option2:
					btProfile = (byte) 0xD2;
					break;
				case id.set_profile_option3:
					btProfile = (byte) 0xD3;
					break;
				default:
					return;
				}
				mReaderHelper.setRfLinkProfile(m_curReaderSetting.btReadId, btProfile);
				m_curReaderSetting.btRfLinkProfile = btProfile;
			}
		}
	};
	
	private void updateView() {
		Log.v("ubx","get link profile:" + m_curReaderSetting.btRfLinkProfile);
		if ((m_curReaderSetting.btRfLinkProfile & 0xFF) == 0xD0) {
			mGroupProfile.check(id.set_profile_option0);
		} else if ((m_curReaderSetting.btRfLinkProfile & 0xFF) == 0xD1) {
			mGroupProfile.check(id.set_profile_option1);
		} else if ((m_curReaderSetting.btRfLinkProfile & 0xFF) == 0xD2) {
			mGroupProfile.check(id.set_profile_option2);
		} else if ((m_curReaderSetting.btRfLinkProfile & 0xFF) == 0xD3) {
			mGroupProfile.check(id.set_profile_option3);
		}
	}

	private RXObserver mObserver = new RXObserver() {
		@Override
		protected void onExeCMDStatus(final byte cmd, final byte status) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (cmd == CMD.GET_RF_LINK_PROFILE || cmd == CMD.SET_RF_LINK_PROFILE) {
						updateView();
					}
					String strLog = FormatUtils.format(cmd, status);
					mLogList.writeLog(strLog, status);
				}
			});
		}
	};

	@Override
	protected void onStart() {
		super.onStart();
		mReaderHelper.registerObserver(mObserver);
	}

	@Override
	protected void onResume() {
		mReaderHelper.startWith();
		super.onResume();
	}

	@Override
	protected void onStop() {
		super.onStop();
		mReaderHelper.unRegisterObserver(mObserver);
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (mLogList.tryClose()) return true;
		}

		return super.onKeyDown(keyCode, event);
	}
}

