package com.udroid.uhf.activity.setpage;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.rfid.RFIDReaderHelper;
import com.rfid.bean.MessageTran;
import com.rfid.rxobserver.RXObserver;
import com.rfid.rxobserver.ReaderSetting;
import com.udroid.uhf.R;
import com.udroid.uhf.activity.BaseActivity;
import com.udroid.uhf.adapter.MaskMapAdapter;
import com.udroid.uhf.dialog.SpinerPopWindow;
import com.udroid.uhf.widget.LogList;
import com.util.StringTool;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 6/15/2017.
 */

public class PageReaderMaskTag extends BaseActivity {
    //fixed by lei.li 2016/11/09
    //private LogList mLogList;
    private LogList mLogList;
    //fixed by lei.li 2016/11/09

    private TextView mSetStartAdd, mSetMaskLen, mSetMaskValue;
    private Spinner mSelectMaskNo;

    private ListView mListView;
    //private TextView mRefreshButton;
    private List<MaskMapAdapter.MaskMap> mapList = new ArrayList<MaskMapAdapter.MaskMap>();
    private MaskMapAdapter mMaskMapAdapter;

    private List<String> mAccessList;
    private Spinner mMaskNo, mSetTarget, mSetAction, mSetMembank, mGetTarget, mGetAction, mGetMembank;

    private SpinerPopWindow mSpinerPopWindow;
    private List<String> listMap = new ArrayList<String>();
    private String[] listMapRes = new String[]{"Mask All", "Mask No.1", "Mask No.2", "Mask No.3", "Mask No.4", "Mask No.5"};

    Button mSet, mGet, mClear;


    private RFIDReaderHelper mReaderHelper;

    private int mPos = -1;

    private static ReaderSetting m_curReaderSetting;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_reader_mask_tag);
        mContext = getApplicationContext();
        try {
            mReaderHelper = RFIDReaderHelper.getDefaultHelper();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        mAccessList = new ArrayList<String>();

        m_curReaderSetting = ReaderSetting.newInstance();

        mLogList = (LogList) findViewById(R.id.log_list);
        mListView = (ListView) findViewById(R.id.get_mask);
        mMaskMapAdapter = new MaskMapAdapter(mContext, mapList);
        mListView.setAdapter(mMaskMapAdapter);

        initView();

//		mRefreshButton = (TextView) findViewById(R.id.refresh);
//		mRefreshButton.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View arg0) {
//				refresh();
//			}
//		});
    }

    void initView() {
        mSetStartAdd = (TextView) findViewById(R.id.mask_start_address);
        mSetMaskLen = (TextView) findViewById(R.id.mask_len);
        mSetMaskValue = (TextView) findViewById(R.id.mask_value);

        /*mGetStartAdd = (TextView) findViewById(R.id.get_mask_start_address);
        mGetMaskLen = (TextView) findViewById(R.id.get_mask_len);
        mGetMaskValue = (TextView) findViewById(R.id.mask_value);*/
        mMaskNo = (Spinner) findViewById(R.id.mask_no);
        mSetTarget = (Spinner) findViewById(R.id.mask_target);
        mSetAction = (Spinner) findViewById(R.id.mask_action);
        mSetMembank = (Spinner) findViewById(R.id.mask_membank);

        mSelectMaskNo = (Spinner) findViewById(R.id.select_mask_no);

       /* mGetTarget = (Spinner) findViewById(R.id.get_mask_target);
        mGetAction = (Spinner) findViewById(R.id.get_mask_action);
        mGetMembank = (Spinner) findViewById(R.id.get_mask_membank);*/

        mSet = (Button) findViewById(R.id.set_mask);
        mGet = (Button) findViewById(R.id.get_mask_button);
        mClear = (Button) findViewById(R.id.clear_mask);

        mSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMask();
            }
        });

        mGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMask();
            }
        });

        mClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear();
            }
        });

        //mClear = (Button)findViewById(R.id.);
    }

    private void setMask() {

        byte maskNo = (byte) (mMaskNo.getSelectedItemId() + 1);
        byte target = (byte) mSetTarget.getSelectedItemId();
        byte action = (byte) mSetAction.getSelectedItemId();
        byte membank = (byte) mSetMembank.getSelectedItemId();

        boolean lenVaild = (mSetStartAdd.getText().length() == 2) ? ((mSetMaskLen.getText().length() == 2) ? true : false) : false;
        if (!lenVaild) {
            Toast.makeText(mContext, "Mask start address and mask length must be 00 - FF", Toast.LENGTH_SHORT).show();
            return;
        }
        byte nStartAdd = (byte) 0xFF;
        byte nMaskLen = (byte) 0xFF;
        byte[] nMaskValue;
        String[] tmp1 = StringTool.stringToStringArray(mSetStartAdd.getText().toString().trim(), 2);
        String[] tmp2 = StringTool.stringToStringArray(mSetMaskLen.getText().toString().trim(), 2);
        nStartAdd = StringTool.stringArrayToByteArray(tmp1, tmp1.length)[0];
        nMaskLen = StringTool.stringArrayToByteArray(tmp2, tmp2.length)[0];
        String str = mSetMaskValue.getText().toString();
        if (str.length() == 0 || str == null || str == "" || str.length() % 2 != 0) {
            Toast.makeText(mContext, "Mask value format invalid!", Toast.LENGTH_SHORT).show();
            return;
        }
        String[] tmp = StringTool.stringToStringArray(mSetMaskValue.getText().toString().trim(), 2);
        nMaskValue = StringTool.stringArrayToByteArray(tmp, tmp.length);
        mReaderHelper.setTagMask((byte) 0x01, maskNo, target, action, membank, nStartAdd, nMaskLen, nMaskValue);
    }

    private void clear() {
        byte maskNo = (byte) mSelectMaskNo.getSelectedItemId();
        mReaderHelper.clearTagMask((byte) 0xFF, maskNo);
    }

    private void getMask() {
        mapList.clear();
        mMaskMapAdapter.notifyDataSetChanged();
        mReaderHelper.getTagMask((byte) 0xFF);
    }

    private void updateGetValue(byte[] btAryData) {
        Log.d("Return value", StringTool.byteArrayToString(btAryData, 0, btAryData.length));
        MaskMapAdapter.MaskMap map = new MaskMapAdapter.MaskMap();
        map.mMaskNo = String.format("%02X", btAryData[0]);
        byte nTarget = btAryData[2];
        switch (nTarget) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            default:
                break;

        }
        map.mTarget = String.format("%02X", btAryData[2]);
        map.mAction = String.format("%02X", btAryData[3]);
        map.mMembank = String.format("%02X", btAryData[4]);
        map.mStartMaskAdd = String.format("%02X", btAryData[5]);
        map.mMaskLen = String.format("%02X", btAryData[6]);
        byte[] tmp = new byte[btAryData.length - 8];
        System.arraycopy(btAryData, 7, tmp, 0, tmp.length);
        map.mMaskValue = StringTool.byteArrayToString(tmp, 0, tmp.length);
        mapList.add(map);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMaskMapAdapter.notifyDataSetChanged();
            }
        });
    }

    private RXObserver mObserver = new RXObserver() {

        @Override
        protected void onConfigTagMask(MessageTran msgTran) {
            String strCmd = "Operate Mask";
            byte[] btAryData = msgTran.getAryData();
            String strErrorCode = "";
            if (btAryData.length == 1) {
                if (btAryData[0] == (byte) 0x10) {
                    writeLog("Command execute success", 0x10);
                    return;
                } else if (btAryData[1] == (byte) 0x41) {
                    strErrorCode = "Invaild parameter";
                } else {
                    strErrorCode = "Unknown Error";
                }
            } else {
                if (btAryData.length > 7) {

                    updateGetValue(btAryData);
                    writeLog("Get tag mask sucess", 0x10);
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return;
                }
            }
            String strLog = strCmd + "Failure, failure cause: " + strErrorCode;
            writeLog(strLog, 0x11);
        }
    };

    private void writeLog(final String message, final int type) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLogList.writeLog(message, type);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mReaderHelper.registerObserver(mObserver);
    }

    @Override
    protected void onResume() {
        mReaderHelper.startWith();
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mReaderHelper.unRegisterObserver(mObserver);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mLogList.tryClose()) return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
