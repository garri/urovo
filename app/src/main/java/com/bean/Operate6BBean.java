package com.bean;

/**
 * @author naz
 * Email 961057759@qq.com
 * Date 2019/12/28
 */
public class Operate6BBean {
    /**
     * Antenna ID
     */
    private byte btAntId;
    /**
     * UID value
     */
    private String uid;
    /**
     * Total tags operated
     */
    private int total;

    public Operate6BBean(byte btAntId, String uid) {
        this.btAntId = btAntId;
        this.uid = uid;
        this.total = 1;
    }

    public byte getBtAntId() {
        return btAntId;
    }

    public void setBtAntId(byte btAntId) {
        this.btAntId = btAntId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void addTotal(){
        this.total++;
    }
}
